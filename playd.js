const fsPromises = require('fs').promises;


async function getGameData(fileName) {
    const data = await fsPromises.readFile(fileName)
        .catch(() => {
            console.error(`Error: Unable to read wishlist file '${fileName}'`);
            process.exit(1);
        });

    return JSON.parse(data);
}


async function getAvailability(id, title, city) {
    let used = 2;
    let found = []

    const res = await fetch(`https://www.gamestop.ca/StoreLocator/GetStoresForStoreLocatorByProduct?value=${id}&skuType=${used}&language=en-CA`);
    const json = await res.json();
    
    const stores = json.filter(s => s.City === city && s.ProductStatus === 'available');
    stores.forEach((s) => {
        found.push({ title: title, name: s.Name, phone: s.Phones });
    });

    return found;
}


function usage(error = false) {
    if (error) {
        console.error('Must provide a Canadian city and wishlist file!');
    }

    console.log('\nUsage:\n  node playd.js -c <city_name> -f <file_name>');
    console.log('\nExample:\n  node playd.js -c Ottawa -f wishlists/all-platforms.json');
}


function processArgs() {
    if (process.argv.length === 3 && process.argv.indexOf('-h') > -1) {
        usage();
        process.exit(0);
    }

    if (process.argv.length !== 6) {
        usage(error = true);
        process.exit(1);
    }

    const flags = {
        city: process.argv.indexOf('-c'),
        file: process.argv.indexOf('-f')
    };

    for (const [key, value] of Object.entries(flags)) {
        if (!(value > -1)) {
            usage();
            process.exit(1);
        }
    };

    return {
        city: process.argv[flags.city + 1],
        file: process.argv[flags.file + 1]
    };
}


(async () => {

    const args = processArgs();

    const data = await getGameData(args.file);

    let bystore = {};
    await Promise.all(
        data.games.map(async (game) => {
            // only check on games marked for search
            if (game.search) {
                let found = await getAvailability(game.id, game.title, args.city);
                if (found.length) {

                    // aggregate the games by store
                    found.forEach((f) => {
                        let key = `${f.name} - ${f.phone}`;
                        if (!bystore[key]) {
                            bystore[key] = [];
                        }
                        bystore[key].push(f.title);
                    });
                }
            }
        })
    );

    // print out formatted list of games by store
    console.log('\n');
    for (const [key, value] of Object.entries(bystore)) {
        console.log(key);
        value.forEach((v) => {
            console.log(' ', v);
        });
        console.log('\n');
    }

})();