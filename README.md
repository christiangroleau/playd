# playd

![Release](https://img.shields.io/badge/Release-1.2-blue?labelColor=grey&style=flat)
![Node](https://img.shields.io/badge/Node-17.5+-blue?labelColor=grey&style=flat)
![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)


A simple way to search GameStop playd inventory across Canada


## What's the deal?

As a patient gamer, I (almost) always buy games used. Several times a year GameStop puts on great sales for their 'playd' games and I would search through their site looking for deals. What originally started as a [Playwright](https://github.com/microsoft/playwright) web scraper turned into something much simpler. Here's is a quick way to scan GameStop's used inventory.

## Getting started

Provide a Canadian city and a games wishlist file. See `/wishlists` for examples.

```
node playd.js -c Ottawa -f wishlists/all-platforms.json
```

See help to display usage
```
node playd.js -h
```

## Example

As of June 29, 2022 looking for a few FromSoft titles ...

```
node playd.js -c Ottawa -f wishlists/fromsoft.json
```

... and here is what is currently in stock
```
RIDEAU CENTRE - EBX - 613-567-9299
  Sekiro
  Demon's Souls


CHAPMAN MILL MARKETPLACE - BARRHAVEN - 613-843-8320
  Sekiro
  Demon's Souls
  Elden Ring


SMARTCENTRE ORLEANS - 613-824-2766
  Sekiro
  Demon's Souls
  Elden Ring


BAYSHORE SHOPPING CENTRE - EBX - 613-596-1881
  Demon's Souls


THE OTTAWA TRAIN YARDS - 613-860-1881
  Demon's Souls
```

Before heading out I **call to make sure their inventory is accurate**. So far, out of 15 games I've found with this tool, only 1 showed up incorrectly (New Super Mario Bros. U Deluxe for Switch)

## Wishlists

Setting up a wishlist is really easy. Get the SKU (see below) and add each game to the json file. Here is a wishlist for playd copies of `Sekiro`, `Demon's Souls` and *(the best game of all time)* `Elden Ring` on PlayStation.

```
{
    "games": [
        {
            "platform": "PlayStation",
            "title": "Sekiro",
            "id": "751516",
            "search": true,
            "notes": ""
        },
        {
            "platform": "PlayStation",
            "title": "Demon's Souls",
            "id": "877519",
            "search": true,
            "notes": ""
        },
        {
            "platform": "PlayStation",
            "title": "Elden Ring",
            "id": "883888",
            "search": true,
            "notes": ""
        },
    ]
}

```

**Note:**
Game ids are GameStop online SKUs listed in the URL of each game's page. Look up a game on the site and the id appears in the URL after `/Games/`. For example, the id for Astral Chain is 765625 as you can see [here](https://www.gamestop.ca/Switch/Games/765625/astral-chain).


### Extra
My wishlist is always growing and over time it gets messy. I've included a very simple json ordering utility that sorts the games by title.

```
node order.js -f wishlists/unordered.json > wishlists/ordered.json
```

Have fun!