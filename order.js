const fsPromises = require('fs').promises;

async function getGameData(fileName) {
    const data = await fsPromises.readFile(fileName)
        .catch((err) => console.error('Failed to read file', err));

    return JSON.parse(data);
}


function usage() {
    console.log('Usage:')
    console.log('  node order.js -f <file_name>');
}


function processArgs() {
    if (process.argv.length !== 4) {
        usage();
        process.exit(1);
    }

    const flags = {
        file: process.argv.indexOf('-f')
    };

    for (const [key, value] of Object.entries(flags)) {
        if (!(value > -1)) {
            usage();
            process.exit(1);
        }
    };

    return {
        file: process.argv[flags.file + 1]
    };
}


(async () => {
    const args = processArgs();
    const data = await getGameData(args.file);

    data.games.sort(function (a, b) {
        var nameA = a.title.toUpperCase();
        var nameB = b.title.toUpperCase();

        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }

        // names must be equal
        return 0;
    });

    console.log(JSON.stringify(data));

})();